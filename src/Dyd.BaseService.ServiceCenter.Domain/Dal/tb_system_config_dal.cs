﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ServiceCenter.Domain.Model;

namespace Dyd.BaseService.ServiceCenter.Domain.Dal
{
    //tb_system_config
    public partial class tb_system_config_dal
    {
        #region C

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_system_config model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tb_system_config(");
            strSql.Append("[key],[value],remark");
            strSql.Append(") values (");
            strSql.Append("@key,@value,@remark");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            par.Add(new ProcedureParameter("@key", model.key));
            par.Add(new ProcedureParameter("@value", model.value));
            par.Add(new ProcedureParameter("@remark", model.remark));
            object obj = conn.ExecuteScalar(strSql.ToString(), par);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_system_config model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tb_system_config set ");

            strSql.Append(" [key] = @key , ");
            strSql.Append(" [value] = @value , ");
            strSql.Append(" remark = @remark  ");
            strSql.Append(" where id=@id ");

            par.Add(new ProcedureParameter("@id", model.id));
            par.Add(new ProcedureParameter("@key", model.key));
            par.Add(new ProcedureParameter("@value", model.value));
            par.Add(new ProcedureParameter("@remark", model.remark));

            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tb_system_config ");
            strSql.Append(" where id=@id");
            par.Add(new ProcedureParameter("id", id));
            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Q
        /// <summary>
        /// 重复规则
        /// </summary>
        public bool IsExists(DbConn conn, tb_system_config model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_system_config s where 1=1");

            stringSql.Append(@" and [key]=@key");
            par.Add(new ProcedureParameter("key", model.key));

            stringSql.Append(@" and id<>@id");
            par.Add(new ProcedureParameter("id", model.id));

            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_system_config Get(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, [key], [value], remark  ");
            strSql.Append(" from tb_system_config ");
            strSql.Append(" where id=@id");
            par.Add(new ProcedureParameter("id", id));
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_system_config> GetPageList(DbConn conn, tb_system_config_Search search, out int totalCount)
        {
            totalCount = 0;
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            IList<tb_system_config> list = new List<tb_system_config>();
            long startIndex = (search.Pno - 1) * search.PageSize + 1;
            long endIndex = startIndex + search.PageSize - 1;
            par.Add(new ProcedureParameter("startIndex", startIndex));
            par.Add(new ProcedureParameter("endIndex", endIndex));

            StringBuilder baseSql = new StringBuilder();
            string sqlWhere = " where 1=1 ";
            baseSql.Append("select row_number() over(order by id desc) as rownum,");
            #region Query
            if (!string.IsNullOrWhiteSpace(search.key))
            {
                sqlWhere += " and [key] like '%'+@key+'%'";
                par.Add(new ProcedureParameter("key", search.key));
            }
            if (!string.IsNullOrWhiteSpace(search.value))
            {
                sqlWhere += " and [value] like '%'+@value+'%'";
                par.Add(new ProcedureParameter("value", search.value));
            }
            if (!string.IsNullOrWhiteSpace(search.remark))
            {
                sqlWhere += " and [remark] like '%'+@remark+'%'";
                par.Add(new ProcedureParameter("remark", search.remark));
            }
            #endregion
            baseSql.Append(" * ");
            baseSql.Append(" FROM tb_system_config with(nolock)");

            string execSql = "select * from(" + baseSql.ToString() + sqlWhere + ")  as s where rownum between @startIndex and @endIndex";
            string countSql = "select count(1) from tb_system_config with(nolock)" + sqlWhere;
            object obj = conn.ExecuteScalar(countSql, par);
            if (obj != DBNull.Value && obj != null)
            {
                totalCount = LibConvert.ObjToInt(obj);
            }
            DataTable dt = conn.SqlToDataTable(execSql, par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        #endregion

        #region private
        public tb_system_config CreateModel(DataRow dr)
        {
            var model = new tb_system_config();
            if (dr.Table.Columns.Contains("id") && dr["id"].ToString() != "")
            {
                model.id = int.Parse(dr["id"].ToString());
            }
            if (dr.Table.Columns.Contains("key") && dr["key"].ToString() != "")
            {
                model.key = dr["key"].ToString();
            }
            if (dr.Table.Columns.Contains("value") && dr["value"].ToString() != "")
            {
                model.value = dr["value"].ToString();
            }
            if (dr.Table.Columns.Contains("remark") && dr["remark"].ToString() != "")
            {
                model.remark = dr["remark"].ToString();
            }

            return model;
        }
        #endregion
    }
}