﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.SystemRuntime
{
    public enum EnumSystemConfigKey
    {
        RedisServer,
        ZooKeeperServer,
        ReportConnectString,
        NodeMinPort,
        NodeMaxPort,
        WebSite
    }

    public enum EnumServiceType
    {
        [Description("None")]
        None = 0,
        [Description("Thrift")]
        Thrift = 1,
        [Description("WCF")]
        WCF = 2,
        [Description("HTTP")]
        HTTP = 3,
    }

    public enum EnumRunState
    {
        [Description("运行")]
        Running = 1,
        [Description("停止")]
        Stop = 2,
    }

    public enum EnumLogType
    {
        [Description("服务")]
        Service = 1,
        [Description("客户端")]
        Client = 2,
        [Description(" 系统")]
        System = 3,
    }
}
