﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.Performance;
using XXF.BaseService.ServiceCenter.Service.Server;
using XXF.BaseService.ServiceCenter.Service.Visitor;

namespace XXF.BaseService.ServiceCenter.Service.Provider
{
    /// <summary>
    /// 服务上下文
    /// </summary>
    public class ServiceContext
    {
        public ServiceProvider ServiceProvider { get; set; }
        public Model.tb_service_model ServiceModel { get; set; }
        public Model.tb_node_model NodeModel { get; set; }
        public ServiceHeatBeatProtect HeatBeatProtect { get; set; }
        public BaseServer Server { get; set; }
        public BasePerformance Performance { get; set; }
    }

}
