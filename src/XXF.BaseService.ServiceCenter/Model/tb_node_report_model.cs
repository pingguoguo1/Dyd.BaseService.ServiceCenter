using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_node_report Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_node_report_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 节点id
        /// </summary>
        public int nodeid { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
        /// <summary>
        /// 错误数量
        /// </summary>
        public int errorcount { get; set; }
        
        /// <summary>
        /// 当前连接数（访问）数量
        /// </summary>
        public int connectioncount { get; set; }
        
        /// <summary>
        /// 访问次数
        /// </summary>
        public long visitcount { get; set; }
        
        /// <summary>
        /// 线程数
        /// </summary>
        public int processthreadcount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public double processcpuper { get; set; }
        
        /// <summary>
        /// 内存大小
        /// </summary>
        public double memorysize { get; set; }
        
    }
}