using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_node Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_node_model
    {

        /*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/

        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 服务id
        /// </summary>
        public int serviceid { get; set; }

        /// <summary>
        /// 服务命名空间（服务唯一标示）
        /// </summary>
        public string servicenamespace { get; set; }

        /// <summary>
        /// 会话id
        /// </summary>
        public long sessionid { get; set; }

        /// <summary>
        /// ip地址
        /// </summary>
        public string ip { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public int port { get; set; }

        /// <summary>
        /// 运行状态
        /// </summary>
        public Byte runstate { get; set; }

        /// <summary>
        /// 节点心跳时间
        /// </summary>
        public DateTime nodeheartbeattime { get; set; }

        /// <summary>
        /// 权重值
        /// </summary>
        public int boostpercent { get; set; }

        /// <summary>
        /// 错误数量
        /// </summary>
        public long errorcount { get; set; }

        /// <summary>
        /// 访问数量
        /// </summary>
        public long connectioncount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int visitcount { get; set; }

        /// <summary>
        /// 线程数
        /// </summary>
        public int processthreadcount { get; set; }

        /// <summary>
        /// cpu时间
        /// </summary>
        public double processcpuper { get; set; }

        /// <summary>
        /// 内存大小
        /// </summary>
        public double memorysize { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public double filesize { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }

        /// <summary>
        /// 接口版本号
        /// </summary>
        public double interfaceversion { get; set; }

        /// <summary>
        /// 协议json
        /// </summary>
        public String protocoljson { get; set; }
        
    }
}