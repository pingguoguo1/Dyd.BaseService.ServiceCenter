﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XXF.BaseService.ServiceCenter.Service.Protocol;

namespace Dyd.BaseService.ServiceCenter.Web.Models
{
    public class ServiceDoc
    {
        public ServiceDoc()
        {
            ServiceProtocalList = new List<ServiceProtocal>();
        }
        public string ServiceName { get; set; }

        public int ServiceId { get; set; }

        /// <summary>
        ///  服务命名空间（服务唯一标示）
        /// </summary>
        public string ServiceNamespace { get; set; }        

        public List<ServiceProtocal> ServiceProtocalList { get; set; }
    }
}